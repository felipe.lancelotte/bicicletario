package totem;

import java.util.List;

import tranca.Tranca;

public class Totem {
    public int id;
    public String localizacao;
    public List<Tranca> trancas;

    public Totem(int id, String localizacao){
        this.id = id;
        this.localizacao = localizacao;
    }

    public void adicionarTranca(Tranca t){
      trancas.add(t);
    }

    public void removerTranca(int idTranca){
      for (Tranca tranca : trancas) {
        if(tranca.id == idTranca){
          trancas.remove(tranca);
        }  
      }
    }
}
