package totem;

public class NewTotemRequest {
    public int id;
    public String localizacao;

    public NewTotemRequest() {
    }

    public NewTotemRequest(int id, String localizacao) {
        this.id = id;
        this.localizacao = localizacao;
    }

}