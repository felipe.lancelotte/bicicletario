package totem;

import equipamento.ErrorResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import io.javalin.plugin.openapi.annotations.*;

public class TotemController {
    
    static final String TOTEM_NAO_ENCONTRADO = "Totem não encontrado";

    private TotemController() {
        throw new IllegalStateException("Controlador de Totem");
    }

    @OpenApi(
            summary = "Adiciona um novo Totem para a rede de totens",
            operationId = "adicionarTotem",
            path = "/totem",
            method = HttpMethod.POST,
            tags = {"Totem"},
            requestBody = @OpenApiRequestBody(content = {@OpenApiContent(from = NewTotemRequest.class)}),
            responses = {
                    @OpenApiResponse(status = "200"),
                    @OpenApiResponse(status = "422", content = {@OpenApiContent(from = ErrorResponse.class)})
            }
    )

    public static void adicionarTotem(Context contexto){
        NewTotemRequest totem = contexto.bodyAsClass(NewTotemRequest.class);
        TotemService.postTotem(totem.localizacao);
        contexto.status(201);
    }



    @OpenApi(
            summary = "Recupara todos os totens cadastrados",
            operationId = "getTotem",
            path = "/totem",
            method = HttpMethod.GET,
            tags = {"Totem"},
            responses = {
                    @OpenApiResponse(status = "200", content = {@OpenApiContent(from = Totem[].class)})
            }
    )

    public static void getTotens (Context contexto){
        contexto.json(TotemService.getTotens());
    }


    @OpenApi(
            summary = "Obter Totem por ID",
            operationId = "getTotemPorID",
            path = "/totem/:totemID",
            method = HttpMethod.GET,
            pathParams = {@OpenApiParam(name = "totemID", type = Integer.class, description = "ID do Totem")},
            tags = {"totem"},
            responses = {
                    @OpenApiResponse(status = "200", content = {@OpenApiContent(from = Totem.class)}),
                    @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)})
            }
    )

    public static void getTotemPorID (Context contexto){
        Totem totem = TotemService.getTotemPorID(validPathParamTotemID(contexto));
        if (totem == null) {
            throw new NotFoundResponse(TOTEM_NAO_ENCONTRADO);
        } else {
            contexto.json(totem);
        }
    }


    @OpenApi(
        summary = "Editar totem pelo ID",
        operationId = "editarTotem",
        path = "/totem/:totemID",
        method = HttpMethod.PATCH,
        pathParams = {@OpenApiParam(name = "totemID", type = Integer.class, description = "ID da totem")},
        tags = {"Totem"},
        requestBody = @OpenApiRequestBody(content = {@OpenApiContent(from = NewTotemRequest.class)}),
        responses = {
                @OpenApiResponse(status = "200"),
                @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)}),
                @OpenApiResponse(status = "422", content = {@OpenApiContent(from = ErrorResponse.class)})
        }
    ) 

    public static void editarTotem (Context contexto){
        Totem totem = TotemService.getTotemPorID(validPathParamTotemID(contexto));
        if (totem == null) {
            throw new NotFoundResponse("Totem não encontrada");
        } else {
            NewTotemRequest newTotem = contexto.bodyAsClass(NewTotemRequest.class);
            TotemService.editarTotem(totem.id, newTotem.localizacao);
            contexto.status(200);
        }
    }



    @OpenApi(
            summary = "Remover totem pelo ID",
            operationId = "removerTotem",
            path = "/totem/:totemID",
            method = HttpMethod.DELETE,
            pathParams = {@OpenApiParam(name = "totemID", type = Integer.class, description = "ID da totem")},
            tags = {"Totem"},
            responses = {
                    @OpenApiResponse(status = "200"),
                    @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)}),
                    @OpenApiResponse(status = "422", content = {@OpenApiContent(from = ErrorResponse.class)})
            }
    )
    public static void removerTotem (Context contexto){
        Totem totem = TotemService.getTotemPorID(validPathParamTotemID(contexto));
        if (totem == null) {
            throw new NotFoundResponse(TOTEM_NAO_ENCONTRADO);
        } else {
            TotemService.removerTotem(totem.id);
            contexto.status(200);
        }
    }



    @OpenApi(
        summary = "Listar trancas de um totem",
        operationId = "getTrancasNoTotem",
        path = "/totem/:totemID/trancas",
        method = HttpMethod.GET,
        pathParams = {@OpenApiParam(name = "totemID", type = Integer.class, description = "ID da totem")},
        tags = {"Totem"},
        responses = {
                @OpenApiResponse(status = "200"),
                @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)}),
                @OpenApiResponse(status = "422", content = {@OpenApiContent(from = ErrorResponse.class)})
        }
    )
    
    public static void getTrancasNoTotem(Context contexto){
        Totem totem = TotemService.getTotemPorID(validPathParamTotemID(contexto));
        if(totem == null){
            throw new NotFoundResponse(TOTEM_NAO_ENCONTRADO);
        } else{
            TotemService.listarTrancas(totem.id);
        }
    }



    @OpenApi(
        summary = "Listar bicicletas de um totem",
        operationId = "getBicicletasNoTotem",
        path = "/totem/:totemID/bicicletas",
        method = HttpMethod.GET,
        pathParams = {@OpenApiParam(name = "totemID", type = Integer.class, description = "ID da totem")},
        tags = {"Totem"},
        responses = {
                @OpenApiResponse(status = "200"),
                @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)}),
                @OpenApiResponse(status = "422", content = {@OpenApiContent(from = ErrorResponse.class)})
        }
    )

    public static void getBicicletasNoTotem(Context contexto){
        Totem totem = TotemService.getTotemPorID(validPathParamTotemID(contexto));
        if(totem == null){
            throw new NotFoundResponse(TOTEM_NAO_ENCONTRADO);
        } else{
            TotemService.listarBicicletas(totem.id);
        }
    }


    
    public static int validPathParamTotemID(Context contexto){
        return contexto.pathParam("totemID", Integer.class).check(id -> id>0).get();
    }
}
