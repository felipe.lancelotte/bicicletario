package totem;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import bicicleta.Bicicleta;
import io.javalin.http.NotFoundResponse;

import tranca.Tranca;
import tranca.TrancaService;
public class TotemService {
    
    final static String TOTEM_NAO_ENCONTRADO = "Totem não encontrado";

    private static Map<Integer, Totem> totens = new HashMap<>();
    private static AtomicInteger lastId;

    static{
        totens.put(0, new Totem(0, "Rua Bonita 209"));
        totens.put(1, new Totem(1, "Rua da Boa Sorte 408"));
        lastId = new AtomicInteger(totens.size());
    }

    private TotemService() {
        throw new IllegalStateException("Serviço de Totem");
      }

    public static void postTotem(String localizacao){
        int id = lastId.incrementAndGet();
        totens.put(id, new Totem(id, localizacao));
    }

    public static Collection<Totem> getTotens(){
        return totens.values();
    }

    public static Totem getTotemPorID (int id){
        return totens.get(id);
    }

    public static void editarTotem (int id, String localizacao){
        totens.put(id, new Totem(id, localizacao));
    }

    public static void removerTotem (int id){
        totens.remove(id);
    }

    public static void adicionarTrancaNoTotem(int totemID, Tranca tranca){
        Totem totem = getTotemPorID(totemID);
        if(totem != null && tranca != null){
           totem.adicionarTranca(tranca);
        } else if(tranca == null){
            throw new NotFoundResponse("A Tranca não foi encontrada");
        } else {
            throw new NotFoundResponse(TOTEM_NAO_ENCONTRADO);
        }
    }

    public static void removerTrancaNoTotem(int totemID, int trancaID){
        Totem totem = getTotemPorID(totemID);
        if(totem != null){
            totem.removerTranca(trancaID);
         } else {
            throw new NotFoundResponse(TOTEM_NAO_ENCONTRADO);
        }
    }

    public static List<Tranca> listarTrancas(int totemID){
        Totem totem = getTotemPorID(totemID);
        if(totem != null){
            return totem.trancas;
        }else{
            throw new NotFoundResponse(TOTEM_NAO_ENCONTRADO);
        }
    }

    public static List<Bicicleta> listarBicicletas(int totemID){
        Totem totem = getTotemPorID(totemID);
        List<Bicicleta> bicicletasNoTotem =  new LinkedList<Bicicleta>();
        
        if(totem != null){
            int contBikes = 0;
            for (Tranca tranca : totem.trancas) {
                if(tranca != null){
                    bicicletasNoTotem.add(TrancaService.getBicicletaNaTranca(tranca.id));
                    contBikes++;
                }
            }
            if(contBikes>0){
                return bicicletasNoTotem;
            } else {
                throw new NotFoundResponse("Não há bicicletas no totem");
            }
        } else {
            throw new NotFoundResponse(TOTEM_NAO_ENCONTRADO);
        }
    }
}
