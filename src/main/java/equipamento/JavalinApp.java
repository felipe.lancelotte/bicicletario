package equipamento;

import io.javalin.Javalin;
import io.javalin.plugin.openapi.OpenApiOptions;
import io.javalin.plugin.openapi.OpenApiPlugin;
import io.javalin.plugin.openapi.ui.ReDocOptions;
import io.javalin.plugin.openapi.ui.SwaggerOptions;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.models.info.Info;

import static io.javalin.apibuilder.ApiBuilder.*;

import bicicleta.BicicletaController;
import totem.TotemController;
import tranca.TrancaController;

public class JavalinApp {
    int port = 7000;

   private Javalin app = 
    		Javalin.create(config -> {
                config.registerPlugin(getConfiguredOpenApiPlugin());
                config.defaultContentType = "application/json";
            }).routes(() -> {


                path("totem", () -> {
                    get(TotemController::getTotens);
                    post(TotemController::adicionarTotem);

                    path("/{totemID}", () -> {
                    	get(TotemController::getTotemPorID);  
                        delete(TotemController::removerTotem);
                        patch(TotemController::editarTotem);              		

                        path("/trancas", () ->{
                            get(TotemController::getTrancasNoTotem);
                        });

                        path("/bicicletas", () ->{
                            get(TotemController::getBicicletasNoTotem);
                        });

                    });       
                    
                }); 



                path("tranca", () -> {
                    get(TrancaController::getTrancas);
                    post(TrancaController::adicionarTranca);

                    path("/{trancaID}", () -> {
                    	get(TrancaController::getTrancaPorID); 
                        patch(TrancaController::editarTranca);
                        delete(TrancaController::removerTranca);

                        path("/bicicleta", () ->{
                            patch(TrancaController::getBicletaNaTranca);
                        });

                        path("/statusTranca/{acao}", () -> {                
                            patch(TrancaController::alterarStatusTranca);                                   
                        });
                    });
                });



                path("bicicleta", ()->{
                    post(BicicletaController::cadastrarBicicleta);
                    get(BicicletaController::getBicicletas);

                    path("/{bicicletaID}", () ->{
                        get(BicicletaController::getBicicletaPorID);
                        delete(BicicletaController::removerBicicleta);
                        patch(BicicletaController::editarBicicleta);

                        path("/{statusBicicleta}", ()->{
                            patch(BicicletaController::alterarStatusBicicleta);
                        });
                    });

                    path("/integrarNaRede", () ->{
                        post(BicicletaController::integrarBicicletaNaRede);
                    });

                    path("/removerDaRede", () ->{
                        post(BicicletaController::retirarBicicletaDaRede);
                    });
                });          
                
              
            });
   
   public void start (int port) {
	   this.app.start(port);
   }
   
   public void stop() {
	   this.app.stop();
   }

    static OpenApiPlugin getConfiguredOpenApiPlugin() {
        Info info = new Info().version("1.0").description("Equipamento API");
        OpenApiOptions options = new OpenApiOptions(info)
                .activateAnnotationScanningFor("Tranca", "Totem", "Bicicleta")
                .path("/swagger-docs") // endpoint for OpenAPI json
                .swagger(new SwaggerOptions("/swagger-ui")) // endpoint for swagger-ui
                .reDoc(new ReDocOptions("/redoc")) // endpoint for redoc
                .defaultDocumentation(doc -> {
                    doc.json("500", ApiResponse.class);
                    doc.json("503", ApiResponse.class);
                });
        return new OpenApiPlugin(options);
    }
   
}
