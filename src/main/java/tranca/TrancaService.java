package tranca;

import bicicleta.Bicicleta;
import io.javalin.http.NotFoundResponse;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class TrancaService {
    
    static final String STATUS_TRANCADA = "EM_USO";
    static final String STATUS_DESTRANCADA = "DISPONIVEL";

    private static Map<Integer, Tranca> trancas = new HashMap<>();
    private static AtomicInteger lastId;

    static{
        trancas.put(0, new Tranca(0, 1000, "Rua Bonita 209", "2021", "Modelo1", STATUS_TRANCADA));
        trancas.put(1, new Tranca(1, 1001, "Rua da Boa Sorte 408", "2021", "Modelo2", STATUS_DESTRANCADA));
        lastId = new AtomicInteger(trancas.size());
    }

    private TrancaService() {
        throw new IllegalStateException("Serviço de Tranca");
      }

    public static void postTranca(int numero, String localizacao, String ano, String modelo, String status){
        int id = lastId.incrementAndGet();
        trancas.put(id, new Tranca(id, numero, localizacao, ano, modelo, status));
    }

    public static Collection<Tranca> getTrancas(){
        return trancas.values();
    }

    public static Tranca getTrancaPorID(int id){
        return trancas.get(id);
    }

    public static void editarTranca(int id, int numero, String localizacao, String ano, String modelo, String status){
        trancas.put(id, new Tranca(id, numero, localizacao, ano, modelo, status));
    }

    public static void alterarStatusTranca(int id, String status){
        Tranca newTranca = getTrancaPorID(id);
        newTranca.status = status;
        trancas.put(id, newTranca);
    }

    public static void removerTranca(int id){
        trancas.remove(id);
    }

    public static Bicicleta getBicicletaNaTranca(int idTranca){
        Tranca tranca = getTrancaPorID(idTranca);
        if(tranca != null && tranca.status.equals(STATUS_TRANCADA)){
            return tranca.getBicicleta(idTranca);
        } else if(tranca == null){
            throw new NotFoundResponse("Tranca não encontrada");
        } else {
            throw new NotFoundResponse("Não há uma bicicleta na Tranca");
        }
    }

    public static void adicionarBicicletaNaTranca(int idTranca, Bicicleta bicicleta){
        Tranca tranca = getTrancaPorID(idTranca);
        if(tranca != null && tranca.status.equals(STATUS_TRANCADA)){
           tranca.adicionarBicicleta(bicicleta);
        } else if(tranca == null){
            throw new NotFoundResponse("A Tranca não foi encontrada");
        } else {
            throw new NotFoundResponse("Já há uma bicicleta na Tranca");
        }
    }

    public static void removerBicicletaNaTranca(int idTranca, int idBicicleta){
        Tranca tranca = getTrancaPorID(idTranca);
        if(tranca != null && tranca.status.equals(STATUS_TRANCADA)){
            tranca.removerBicicleta(idBicicleta);
         }  else if(tranca == null){
            throw new NotFoundResponse("A Tranca não foi encontrada");
        } else {
            throw new NotFoundResponse("");
        }
    }

}
