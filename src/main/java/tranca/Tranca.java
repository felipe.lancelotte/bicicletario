package tranca;

import bicicleta.Bicicleta;
import io.javalin.http.NotFoundResponse;
public class Tranca {
    public int id;
    public int numero;
    public String localizacao;
    public String anoDeFabricacao;
    public String modelo;
    public String status;
    public Bicicleta bicicleta;

    public Tranca(int id, int numero, String localizacao, String ano, String modelo, String status){
        this.id = id;
        this.numero = numero;
        this.localizacao = localizacao;
        anoDeFabricacao = ano;
        this.modelo = modelo;
        this.status = status;
    }

    public void adicionarBicicleta(Bicicleta b){
       bicicleta = b;
    }

    public Bicicleta getBicicleta(int idTranca){
        if(idTranca == id){
            return bicicleta;
        }
        else{
            throw new NotFoundResponse("Tranca inválida");
        }
    }

    public void removerBicicleta(int idBicicleta){
        if(bicicleta == null){
            throw new NotFoundResponse("Nao há bicicleta na tranca");
        } else if (bicicleta.id == idBicicleta){
            bicicleta = null;
        } else {
            throw new NotFoundResponse("Bicicleta inválida");
        }
    }
}
