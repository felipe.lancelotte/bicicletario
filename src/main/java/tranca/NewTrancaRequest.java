package tranca;

import bicicleta.Bicicleta;
public class NewTrancaRequest {
    public int id;
    public int numero;
    public String localizacao;
    public String anoDeFabricacao;
    public String modelo;
    public String status;
    public Bicicleta bicicleta;

    public NewTrancaRequest(){}

    public NewTrancaRequest(int numero, String localizacao, String ano, String modelo, String status){
        this.numero = numero;
        this.localizacao = localizacao;
        anoDeFabricacao = ano;
        this.modelo = modelo;
        this.status = status;
    }

}
