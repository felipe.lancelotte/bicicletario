package tranca;

import totem.Totem;
import totem.TotemService;
import totem.TotemController;

import equipamento.ErrorResponse;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import io.javalin.plugin.openapi.annotations.*;

public class TrancaController {

    static final String TOTEM_NAO_ENCONTRADO = "totemNaoEncontrado";
    static final String TRANCA_NAO_ENCONTRADA = "trancaNaoEncontrada";
    static final String BICICLETA_NAO_ENCONTRADA = "bicicletaNaoEncontrada";
    static final String ACAO_REALIZADA = "acao";
    static final String ACAO_INVALIDA = "Ação inválida";

    private TrancaController() {
        throw new IllegalStateException("Controlador de Tranca");
    }

    
    @OpenApi(
        summary = "Cadastra tranca",
        operationId = "adicionarTranca",
        path = "/tranca",
        method = HttpMethod.POST,
        tags = {"Tranca"},
        requestBody = @OpenApiRequestBody(content = {@OpenApiContent(from = NewTrancaRequest.class)}),
        responses = {
                @OpenApiResponse(status = "200"),
                @OpenApiResponse(status = "422", content = {@OpenApiContent(from = ErrorResponse.class)})
        }
    )

    public static void adicionarTranca (Context contexto){
        NewTrancaRequest tranca = contexto.bodyAsClass(NewTrancaRequest.class);
        TrancaService.postTranca(tranca.numero, tranca.localizacao, tranca.anoDeFabricacao, tranca.modelo, tranca.status);
    }



    @OpenApi(
            summary = "Recupara todas as trancas cadastradas",
            operationId = "getTrancas",
            path = "/tranca",
            method = HttpMethod.GET,
            tags = {"Tranca"},
            responses = {
                    @OpenApiResponse(status = "200", content = {@OpenApiContent(from = Tranca[].class)})
            }
    )

    public static void getTrancas (Context contexto){
        contexto.json(TrancaService.getTrancas());
    }



    @OpenApi(
            summary = "Obter tranca por ID",
            operationId = "getTrancaPorID",
            path = "/tranca/:trancaID",
            method = HttpMethod.GET,
            pathParams = {@OpenApiParam(name = "trancaID", type = Integer.class, description = "ID da tranca")},
            tags = {"Tranca"},
            responses = {
                    @OpenApiResponse(status = "200", content = {@OpenApiContent(from = Tranca.class)}),
                    @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)})
            }
    )

    public static void getTrancaPorID (Context contexto){
        Tranca tranca = TrancaService.getTrancaPorID(validPathParamTrancaID(contexto));
        if (tranca == null) {
            throw new NotFoundResponse(TRANCA_NAO_ENCONTRADA);
        } else {
            contexto.json(tranca);
        }
    }



    @OpenApi(
        summary = "Editar tranca pelo ID",
        operationId = "editarTranca",
        path = "/tranca/:trancaID",
        method = HttpMethod.PATCH,
        pathParams = {@OpenApiParam(name = "trancaID", type = Integer.class, description = "ID da tranca")},
        tags = {"Tranca"},
        requestBody = @OpenApiRequestBody(content = {@OpenApiContent(from = NewTrancaRequest.class)}),
        responses = {
                @OpenApiResponse(status = "200"),
                @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)}),
                @OpenApiResponse(status = "422", content = {@OpenApiContent(from = ErrorResponse.class)})
        }
    ) 

    public static void editarTranca (Context contexto){
        Tranca tranca = TrancaService.getTrancaPorID(validPathParamTrancaID(contexto));
        if (tranca == null) {
            throw new NotFoundResponse(TRANCA_NAO_ENCONTRADA);
        } else {
            NewTrancaRequest newTranca = contexto.bodyAsClass(NewTrancaRequest.class);
            TrancaService.editarTranca(tranca.id, newTranca.numero, newTranca.localizacao, newTranca.anoDeFabricacao, newTranca.modelo, newTranca.status);
            contexto.status(200);
        }
    }



    @OpenApi(
            summary = "Remover tranca pelo ID",
            operationId = "removerTranca",
            path = "/tranca/:trancaID",
            method = HttpMethod.DELETE,
            pathParams = {@OpenApiParam(name = "trancaID", type = Integer.class, description = "ID da tranca")},
            tags = {"Tranca"},
            responses = {
                    @OpenApiResponse(status = "200"),
                    @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)}),
                    @OpenApiResponse(status = "422", content = {@OpenApiContent(from = ErrorResponse.class)})
            }
    )
    public static void removerTranca (Context contexto){
        Tranca tranca = TrancaService.getTrancaPorID(validPathParamTrancaID(contexto));
        if (tranca == null) {
            throw new NotFoundResponse(TRANCA_NAO_ENCONTRADA);
        } else {
            TrancaService.removerTranca(tranca.id);
            contexto.status(200);
        }
    }


    
    @OpenApi(
        summary = "Obter bicicleta na tranca",
        operationId = "getBicicletaNaTranca",
        path = "/tranca/:trancaID/bicicleta",
        method = HttpMethod.PATCH,
        pathParams = {@OpenApiParam(name = "trancaID", type = Integer.class, description = "ID da tranca")
        },
        tags = {"Tranca"},
        responses = {
                @OpenApiResponse(status = "200"),
                @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)}),
                @OpenApiResponse(status = "422", content = {@OpenApiContent(from = ErrorResponse.class)})
        }
    )

    public static void getBicletaNaTranca (Context contexto){
        Tranca tranca = TrancaService.getTrancaPorID(TotemController.validPathParamTotemID(contexto));
        if (tranca == null) {
            throw new NotFoundResponse(TRANCA_NAO_ENCONTRADA);
        } else {
            contexto.json(TrancaService.getBicicletaNaTranca(TotemController.validPathParamTotemID(contexto)));
            contexto.status(200);
        }
    }



    @OpenApi(
        summary = "Alterar status da tranca",
        operationId = "alterarStatusTranca",
        path = "/tranca/:trancaID/statusTranca/:acao",
        method = HttpMethod.PATCH,
        pathParams = {@OpenApiParam(name = "trancaID", type = Integer.class, description = "ID da tranca"),
                      @OpenApiParam(name = "acao", type = String.class, description = "Ação sendo realizada")
        },
        tags = {"Tranca"},
        responses = {
                @OpenApiResponse(status = "200"),
                @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)}),
                @OpenApiResponse(status = "422", content = {@OpenApiContent(from = ErrorResponse.class)})
        }
    )

    public static void alterarStatusTranca (Context contexto){
        Tranca tranca = TrancaService.getTrancaPorID(validPathParamTrancaID(contexto));
	        if (tranca == null) {
	            throw new NotFoundResponse(TRANCA_NAO_ENCONTRADA);
	        } else if(!acaoValida(contexto)){
	        		throw new BadRequestResponse(ACAO_INVALIDA);
	        	
	        } else {
	            TrancaService.alterarStatusTranca(tranca.id, contexto.queryParam(ACAO_REALIZADA));
	            contexto.status(200);
	        }
    }

    public static boolean acaoValida (Context contexto){
        if(
            (contexto.queryParam(ACAO_REALIZADA) == null) ||
            !contexto.queryParam(ACAO_REALIZADA).equals("DESTRANCAR") &&
            !contexto.queryParam(ACAO_REALIZADA).equals("TRANCAR") 
        ){
            return false;
        } else {
            return true;
        }
    }


     
    @OpenApi(
            summary = "Colocar uma tranca nova ou retornando de reparo de volta na rede de totens",
            operationId = "integrarTrancaNaRede",
            path = "/tranca/integrarNaRede",
            method = HttpMethod.POST,
            tags = {"Bicicleta"},
            pathParams = {@OpenApiParam(name = "totemID", type = Integer.class, description = "ID do Totem"),
                      @OpenApiParam(name = "trancaID", type = Integer.class, description = "ID da Tranca")
            },
            responses = {
                    @OpenApiResponse(status = "200"),
                    @OpenApiResponse(status = "422", content = {@OpenApiContent(from = ErrorResponse.class)})
            }
    )

    public static void integrarTrancaNaRede (Context contexto){
        Tranca tranca = TrancaService.getTrancaPorID(validPathParamTrancaID(contexto));
        Totem totem = TotemService.getTotemPorID(TotemController.validPathParamTotemID(contexto));
        if (totem == null) {
            throw new NotFoundResponse(TOTEM_NAO_ENCONTRADO);
        } else if(tranca == null) {
            throw new NotFoundResponse(TRANCA_NAO_ENCONTRADA);
        } else{
            totem.adicionarTranca(tranca);
        }
    }



    @OpenApi(
        summary = "Retirar uma tranca para aposentadoria ou reparo",
        operationId = "retirarTrancaDaRede",
        path = "/tranca/retirarTrancaDaRede",
        method = HttpMethod.POST,
        tags = {"Bicicleta"},
        pathParams = {@OpenApiParam(name = "totemID", type = Integer.class, description = "ID do Totem"),
                  @OpenApiParam(name = "trancaID", type = Integer.class, description = "ID da Tranca")
        },
        responses = {
                @OpenApiResponse(status = "200"),
                @OpenApiResponse(status = "422", content = {@OpenApiContent(from = ErrorResponse.class)})
        }
)

    public static void retirarTrancaDaRede(Context contexto){
        Tranca tranca = TrancaService.getTrancaPorID(TrancaController.validPathParamTrancaID(contexto));
        Totem totem = TotemService.getTotemPorID(TotemController.validPathParamTotemID(contexto));

        if (totem == null) {
            throw new NotFoundResponse(TOTEM_NAO_ENCONTRADO);
        } else if(tranca == null){
                throw new BadRequestResponse(TRANCA_NAO_ENCONTRADA);
        } else {
            TotemService.removerTrancaNoTotem(tranca.id, totem.id);
            contexto.status(200);
        }
    }

    public static int validPathParamTrancaID(Context contexto){
        return contexto.pathParam("trancaID", Integer.class).check(id -> id>0).get();
    }
   

}
