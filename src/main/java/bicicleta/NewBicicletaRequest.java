package bicicleta;

public class NewBicicletaRequest {
    public int id;
    public String marca;
    public String modelo;
    public String ano;
    public int numero;
    public String status;

    public NewBicicletaRequest(){}

    public NewBicicletaRequest(int id, String marca, String modelo, String ano, int numero, String status){
        this.id = id;
        this.marca = marca;
        this.modelo = modelo;
        this.ano = ano;
        this.numero = numero;
        this.status = status;
    }
}
