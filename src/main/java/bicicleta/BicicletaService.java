package bicicleta;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class BicicletaService {
    
    private static Map<Integer, Bicicleta> bicicletas = new HashMap<>();
    private static AtomicInteger lastId;

    static{
        bicicletas.put(0, new Bicicleta(0, "marca_A", "modelo_A", "2021", 100, "Funcional"));
        lastId = new AtomicInteger(bicicletas.size());
    }

    private BicicletaService() {
        throw new IllegalStateException("Serviço de Bicicleta");
      }

    public static void postBicicleta(String marca, String modelo, String ano, int numero, String status){
        int id = lastId.incrementAndGet();
        bicicletas.put(id, new Bicicleta(id, marca, modelo, ano, numero, status));
    }

    public static Collection<Bicicleta> getBicicletas(){
        return bicicletas.values();
    }

    public static Bicicleta getBicicletaPorID(int id){
        return bicicletas.get(id);
    }

    public static void removerBicicleta(int id){
        bicicletas.remove(id);
    }

    public static void editarBicicleta(int id, String marca, String modelo, String ano, int numero, String status){
        bicicletas.put(id, new Bicicleta(id, marca, modelo, ano, numero, status));
    }

    public static void alterarStatusBicicleta (int id, String status){
        Bicicleta newBicicleta = getBicicletaPorID(id);
        newBicicleta.status = status;
        bicicletas.put(id, newBicicleta);
    }

    public static Bicicleta bicicletaMock(){
        bicicletas.clear();
        bicicletas.put(0, new Bicicleta(0, "marca_A", "modelo_A", "2021", 100, "Funcional"));
        bicicletas.put(1, new Bicicleta(1, "marca_B", "modelo_B", "2021", 100, "Funcional"));
        bicicletas.put(2, new Bicicleta(2, "marca_C", "modelo_C", "2021", 100, "Funcional"));
        return bicicletas.get(bicicletas.size() -1);
    }
}