package bicicleta;

import equipamento.ErrorResponse;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import io.javalin.plugin.openapi.annotations.*;

import tranca.Tranca;
import tranca.TrancaController;
import tranca.TrancaService;

public class BicicletaController {

    static final String BICICLETA_NAO_ENCONTRADA = "Bicicleta não encontrada";
    static final String STATUS_BICICLETA = "statusBicicleta";
    static final String TRANCA_NAO_ENCONTRADA = "trancaNaoEncontrada";


    private BicicletaController() {
        throw new IllegalStateException("Controlador de Bicicleta");
    }
    

    @OpenApi(
            summary = "Cadastra Bicicleta",
            operationId = "cadastrarBicicleta",
            path = "/bicicleta",
            method = HttpMethod.POST,
            tags = {"Bicicleta"},
            requestBody = @OpenApiRequestBody(content = {@OpenApiContent(from = NewBicicletaRequest.class)}),
            responses = {
                    @OpenApiResponse(status = "200"),
                    @OpenApiResponse(status = "422", content = {@OpenApiContent(from = ErrorResponse.class)})
            }
    )

    public static void cadastrarBicicleta(Context contexto) {
        NewBicicletaRequest bicicleta = contexto.bodyAsClass(NewBicicletaRequest.class);
        BicicletaService.postBicicleta(bicicleta.marca, bicicleta.modelo, bicicleta.ano, bicicleta.numero, bicicleta.status);
        contexto.status(200);
    }



    @OpenApi(
            summary = "Recupera todas as bicicletas cadastradas",
            operationId = "getBicicletas",
            path = "/bicicleta",
            method = HttpMethod.GET,
            tags = {"Bicicleta"},
            responses = {
                    @OpenApiResponse(status = "200", content = {@OpenApiContent(from = Bicicleta[].class)})
            }
    )

    public static void getBicicletas(Context contexto){
        contexto.json(BicicletaService.getBicicletas());
    }



    @OpenApi(
            summary = "Obter Bicicleta por ID",
            operationId = "getBicicletaPorID",
            path = "/bicicleta/:bicicletaID",
            method = HttpMethod.GET,
            pathParams = {@OpenApiParam(name = "bicicletaID", type = Integer.class, description = "ID de Bicicleta")},
            tags = {"Bicicleta"},
            responses = {
                    @OpenApiResponse(status = "200", content = {@OpenApiContent(from = Bicicleta.class)}),
                    @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)})
            }
    )

    public static void getBicicletaPorID (Context contexto){
        Bicicleta bicicleta = BicicletaService.getBicicletaPorID(validPathParamBicicletaID(contexto));
        if (bicicleta == null){
            throw new NotFoundResponse(BICICLETA_NAO_ENCONTRADA);
        } else {
            contexto.json(bicicleta);
        }
    }



    @OpenApi(
            summary = "Remover bicicleta pelo ID",
            operationId = "removerBicicleta",
            path = "/bicicleta/:bicicletaID",
            method = HttpMethod.DELETE,
            pathParams = {@OpenApiParam(name = "bicicletaID", type = Integer.class, description = "ID de bicicleta")},
            tags = {"Bicicleta"},
            responses = {
                    @OpenApiResponse(status = "204"),
                    @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)})
            }
    )

    public static void removerBicicleta (Context contexto){
        Bicicleta bicicleta = BicicletaService.getBicicletaPorID(validPathParamBicicletaID(contexto));
        if (bicicleta == null) {
            throw new NotFoundResponse(BICICLETA_NAO_ENCONTRADA);
        } else {
            BicicletaService.removerBicicleta(bicicleta.id);
            contexto.status(204);
        }
    }



    @OpenApi(
            summary = "Editar Bicicleta",
            operationId = "editarBicicleta",
            path = "/bicicleta/:bicicletID",
            method = HttpMethod.PATCH,
            pathParams = {@OpenApiParam(name = "bicicletaID", type = Integer.class, description = "ID de bicicleta")},
            tags = {"Bicicleta"},
            requestBody = @OpenApiRequestBody(content = {@OpenApiContent(from = NewBicicletaRequest.class)}),
            responses = {
                    @OpenApiResponse(status = "200"),
                    @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)}),
                    @OpenApiResponse(status = "422", content = {@OpenApiContent(from = ErrorResponse.class)})
            }
    )

    public static void editarBicicleta (Context contexto){
        Bicicleta bicicleta = BicicletaService.getBicicletaPorID(validPathParamBicicletaID(contexto));
        if (bicicleta == null) {
            throw new NotFoundResponse(BICICLETA_NAO_ENCONTRADA);
        } else {
            NewBicicletaRequest newBicicleta = contexto.bodyAsClass(NewBicicletaRequest.class);
            BicicletaService.editarBicicleta(bicicleta.id, newBicicleta.marca, newBicicleta.modelo, newBicicleta.ano, newBicicleta.numero, newBicicleta.status);
            contexto.status(200);
        }
    }

    

    @OpenApi(
            summary = "Alterar status da bicicleta",
            operationId = "alterarStatusBicicleta",
            path = "/bicicleta/:bicicletID/:statusBicicleta",
            method = HttpMethod.PATCH,
            pathParams = {@OpenApiParam(name = "bicicletaID", type = Integer.class, description = "ID de bicicleta"),
                          @OpenApiParam(name = "statusBicicleta", type = String.class, description = "Status da Bicicleta")
            },
            tags = {"Bicicleta"},
            responses = {
                    @OpenApiResponse(status = "200"),
                    @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)}),
                    @OpenApiResponse(status = "422", content = {@OpenApiContent(from = ErrorResponse.class)})
            }
    )
    
    public static void alterarStatusBicicleta (Context contexto){
        Bicicleta bicicleta = BicicletaService.getBicicletaPorID(validPathParamBicicletaID(contexto));
        if (bicicleta == null){
            throw new NotFoundResponse(BICICLETA_NAO_ENCONTRADA);
        } else if(!statusValido(contexto)){
            throw new BadRequestResponse("Status inválido");
        } else {
            BicicletaService.alterarStatusBicicleta(bicicleta.id, contexto.queryParam(STATUS_BICICLETA));
            contexto.status(200);
        }
    }

    

    public static boolean statusValido (Context contexto){
        if(
            (contexto.queryParam(STATUS_BICICLETA) == null) ||
            !contexto.queryParam(STATUS_BICICLETA).equals("DISPONIVEL") &&
            !contexto.queryParam(STATUS_BICICLETA).equals("EM_USO") &&
            !contexto.queryParam(STATUS_BICICLETA).equals("NOVA") &&
            !contexto.queryParam(STATUS_BICICLETA).equals("APOSENTADA") &&
            !contexto.queryParam(STATUS_BICICLETA).equals("REPARO_SOLICITADO") &&
            !contexto.queryParam(STATUS_BICICLETA).equals("EM_REPARO")
        ){
            return false;
        } else {
            return true;
        }
    }

    private static int validPathParamBicicletaID(Context contexto){
        return contexto.pathParam("bicicletaID", Integer.class).check(id -> id>0).get();
    }


    @OpenApi(
        summary = "Adicionar uma Bicicleta nova ou retornando de reparo de volta",
        operationId = "integrarBicicletaNaRede",
        path = "/bicicleta/integrarNaRede",
        method = HttpMethod.POST,
        pathParams = {@OpenApiParam(name = "trancaID", type = Integer.class, description = "ID da tranca"),
                      @OpenApiParam(name = "bicicletaID", type = Bicicleta.class, description = "ID da Bicicleta")
        },
        tags = {"Bicicleta"},
        responses = {
                @OpenApiResponse(status = "200"),
                @OpenApiResponse(status = "422", content = {@OpenApiContent(from = ErrorResponse.class)})
        }
    )

    public static void integrarBicicletaNaRede(Context contexto){
        Tranca tranca = TrancaService.getTrancaPorID(TrancaController.validPathParamTrancaID(contexto));
        Bicicleta bicicleta = BicicletaService.getBicicletaPorID(BicicletaController.validPathParamBicicletaID(contexto));

	        if (tranca == null) {
	            throw new NotFoundResponse(TRANCA_NAO_ENCONTRADA);
	        } else if(!bicicletaValida(bicicleta)){
	        		throw new BadRequestResponse(BICICLETA_NAO_ENCONTRADA);
	        } else {
	            TrancaService.adicionarBicicletaNaTranca(tranca.id, bicicleta);
	            contexto.status(200);
	        }
    }



    @OpenApi(
        summary = "Retirar bicicleta para reparo ou aposentadoria",
        operationId = "retirarBicicletaDaRede",
        path = "/bicicleta/retirarDaRede",
        method = HttpMethod.POST,
        pathParams = {@OpenApiParam(name = "trancaID", type = Integer.class, description = "ID da tranca"),
                      @OpenApiParam(name = "bicicletaID", type = Integer.class, description = "objeto da Bicicleta")
        },
        tags = {"Tranca"},
        responses = {
                @OpenApiResponse(status = "200"),
                @OpenApiResponse(status = "422", content = {@OpenApiContent(from = ErrorResponse.class)})
        }
    )

    public static void retirarBicicletaDaRede (Context contexto){
        Tranca tranca = TrancaService.getTrancaPorID(TrancaController.validPathParamTrancaID(contexto));
        Bicicleta bicicleta = BicicletaService.getBicicletaPorID(BicicletaController.validPathParamBicicletaID(contexto));

        if (tranca == null) {
            throw new NotFoundResponse(TRANCA_NAO_ENCONTRADA);
        } else if(!bicicletaValida(bicicleta)){
                throw new BadRequestResponse(BICICLETA_NAO_ENCONTRADA);
        } else {
            TrancaService.removerBicicletaNaTranca(tranca.id, bicicleta.id);
            contexto.status(200);
        }
    }



    private static Boolean bicicletaValida(Bicicleta bicicleta){
        if (bicicleta != null){
            return true;
        } else{
            return false;
        }
    }

}
