import bicicleta.*;
/*import totem.*;
import tranca.*;*/

import static org.junit.jupiter.api.Assertions.*;


import org.junit.jupiter.api.Test;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;



public class UnitTests {
    
    @Test
    void testCadastraBicicleta(){
        Bicicleta bicicleta = new Bicicleta(999, "marca", "modelo", "2020", 100, "status");
        HttpResponse<JsonNode> jsonResponse =
             Unirest.post("http://localhost:7010/bicicleta")
                .body(bicicleta).asJson();
        assertEquals(200, jsonResponse.getStatus());
    }

    @Test
    void testGetBicicletas() {
        HttpResponse<JsonNode> jsonResponse =
             Unirest.get("http://localhost:7010/bicicleta")
                .header("accept", "application/json").asJson();
        assertEquals(200, jsonResponse.getStatus());
    }

    @Test
    void testGetBicicletaPorID() {
        int id = (BicicletaService.bicicletaMock()).id;
        HttpResponse<JsonNode> response =
            Unirest.get("http://localhost:7010/bicicleta/"+id).asJson();
        assertEquals(200, response.getStatus());
    }
    
    @Test
    void testRemoverBicicleta(){}


}
